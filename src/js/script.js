$(document).ready(function () {
  // toggleLanguage(); //open-close laguage menu
  // checking if open laguage button
  // $(document).mouseup(function (e) {
  //   var searchLanguage = $('.js-header-lang');
  //   if (!searchLanguage.is(e.target) && searchLanguage.has(e.target).length === 0) {
  //     $('.js-header-lang').removeClass('active');
  //   }
  // });

  $(document).mousemove(function (e) {
    if ($(window).width() > 768) {
      $('.move.move-speed-4').parallax(-200, e);
      $('.move.move-speed-3').parallax(-100, e);
      $('.move.move-speed-2').parallax(-50, e);
      $('.move.move-speed-1').parallax(-20, e);
    }
  });


  $('.js-humburger').click(function () {
    $('.humburger').toggleClass('open');
    $('.header-nav').toggleClass('active');
    $('.header-nav__list').toggleClass('active').delay(600);
    $('body').toggleClass('no-scroll')
  });
  $('.scroll-on-click[href^="#"]').on('click', function (e) {
    e.preventDefault();
    $('.humburger').removeClass('open');
    $('.header-nav').removeClass('active');
    $('.header-nav__list').removeClass('active').delay(600);
    $('body').removeClass('no-scroll')
    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top
    }, 800);
  });


  $('.main-wrapper').removeClass('fade_in');

  $(window).scroll(function () {
    $('.js-scroll').each(function () {
      var sectionPos = $(this).offset().top;
      var windowPos = $(window).scrollTop() + $(window).height() / 1.4;
      if (sectionPos < windowPos) {
        $(this).removeClass('sl_left');
        $(this).removeClass('sl_right');
        $(this).removeClass('sl_top');
      }
    });
  });

  var currentScrollPos = $(this).scrollTop();
  var lastScrollPos = 300;
  if (currentScrollPos > lastScrollPos) {
    $('.js-scroll').removeClass('sl_left');
    $('.js-scroll').removeClass('sl_right');
    $('.js-scroll').removeClass('sl_top');
  }
  setTimeout(function () {
    $('.animate').removeClass('sl_left');
    $('.animate').removeClass('sl_right');
  }, 800);

  $('[data-fancybox]').fancybox({
    toolbar: false,
    smallBtn: true,
    iframe: {
      preload: false
    }
  })
});
// END DOCUMENT READY
$(window).on('load', function () {
  //replace images attribute
  $('img[data-src]').each(function () {
    $(this).attr('src', $(this).attr('data-src')).removeAttr('data-src');
  });
});

function noScroll() {
  $('body').toggleClass('no-scroll');
}

// checkind window sizes
$(window).resize(function () {
  // vacancyItemHieght();
});

// function toggleLanguage() {
//   $('.js-header-lang').click(function () {
//     $('.js-header-lang').toggleClass('active');
//   });
// }

// Load SVG-sprite for all browsers
svg4everybody({});